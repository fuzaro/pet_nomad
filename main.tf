##  Este script Terraform faz a configuração de um cluster (PetCluster) utilizando o tier Always Free da OCI (Oracle Cloud Infraestructure) para validação de conceitos e configurações. 
##  Enfim, gostei tanto de fazer isto, que acredito estar em um estágio estável e interessante para ser compartilhado e com contribuições, evoluir ....
##  

resource oci_core_instance server-001 {
  agent_config {
    are_all_plugins_disabled = "false"
    is_management_disabled   = "false"
    is_monitoring_disabled   = "false"
    plugins_config {
      desired_state = "DISABLED"
      name          = "Vulnerability Scanning"
    }
    plugins_config {
      desired_state = "ENABLED"
      name          = "OS Management Service Agent"
    }
    plugins_config {
      desired_state = "DISABLED"
      name          = "Management Agent"
    }
    plugins_config {
      desired_state = "ENABLED"
      name          = "Custom Logs Monitoring"
    }
    plugins_config {
      desired_state = "ENABLED"
      name          = "Compute Instance Run Command"
    }
    plugins_config {
      desired_state = "ENABLED"
      name          = "Compute Instance Monitoring"
    }
    plugins_config {
      desired_state = "DISABLED"
      name          = "Block Volume Management"
    }
    plugins_config {
      desired_state = "DISABLED"
      name          = "Bastion"
    }
  }
  connection {
    type = "ssh"
    # The default username for our AMI
    user        = "opc"
    host        = self.public_ip
    private_key = file("~/.ssh/id_rsa")
    # The connection will use the local SSH agent for authentication.
  }
  #async = <<Optional value not found in discovery>>
  availability_config {
    #is_live_migration_preferred = <<Optional value not found in discovery>>
    recovery_action = "RESTORE_INSTANCE"
  }
  availability_domain = var.oci_identity_availability_domain
  #capacity_reservation_id = <<Optional value not found in discovery>>
  compartment_id = var.compartment_ocid
  create_vnic_details {
    #assign_private_dns_record = <<Optional value not found in discovery>>
    assign_public_ip = "true"
    assign_private_dns_record = "true"
    defined_tags = {
      "Oracle-Tags.CreatedBy" = "PetCluster"
    }
    display_name = "server-001"
    freeform_tags = {
    }
    hostname_label = "server-001"
    nsg_ids = [
    ]
    private_ip             = var.server_001_priv_ip
    skip_source_dest_check = "false"
    subnet_id              = oci_core_subnet.export_subnet-pet_cluster.id
    #vlan_id = <<Optional value not found in discovery>>
  }
  #dedicated_vm_host_id = <<Optional value not found in discovery>>
  defined_tags = {
    "Oracle-Tags.CreatedBy" = "PetCluster"
  }
  display_name = "server-001"
  extended_metadata = {
  }
  #fault_domain = "FAULT-DOMAIN-1"
  freeform_tags = {
  }
  instance_options {
    are_legacy_imds_endpoints_disabled = "false"
  }
  #ipxe_script = <<Optional value not found in discovery>>
  #is_pv_encryption_in_transit_enabled = <<Optional value not found in discovery>>
  launch_options {
    boot_volume_type                    = "PARAVIRTUALIZED"
    firmware                            = "UEFI_64"
    is_consistent_volume_naming_enabled = "true"
    is_pv_encryption_in_transit_enabled = "true"
    network_type                        = "PARAVIRTUALIZED"
    remote_data_volume_type             = "PARAVIRTUALIZED"
  }
  metadata = {
    "ssh_authorized_keys" = var.ssh_public_key
  }
  #preserve_boot_volume = <<Optional value not found in discovery>>
  shape = var.srv_shape
  shape_config {
    baseline_ocpu_utilization = ""
    memory_in_gbs             = var.srv_mem
    ocpus                     = "1"
  }
  source_details {
    #boot_volume_size_in_gbs = <<Optional value not found in discovery>>
    #kms_key_id = <<Optional value not found in discovery>>
    source_id   = var.srv_source_image_id
    source_type = "image"
  }
  state = "RUNNING"
  provisioner "file" {
    source      = "files"
    destination = "/home/opc"
  }
  provisioner "remote-exec" {
    inline = [
      "sudo yum -y update",
      "sudo systemctl stop firewalld",
      "sudo systemctl disable firewalld",
      "sudo sed -i 's/=enforcing/=disabled/' /etc/selinux/config",
      "sudo yum config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo",
      "sudo sed -i 's/$$releasever/8/g' /etc/yum.repos.d/docker-ce.repo",
      "sudo yum install -y docker-ce.aarch64",
      "sudo systemctl enable docker",
      "sudo yum install -y bind",
      "sudo cp files/dns/named.conf /etc/",  
      "sudo cp files/dns/consul.conf /etc/named/",  
      "sudo systemctl enable named",
      "wget https://releases.hashicorp.com/nomad/1.2.2/nomad_1.2.2_linux_arm64.zip",
      "unzip nomad_1.2.2_linux_arm64.zip",
      "sudo chown root:root nomad",
      "sudo mv nomad /usr/local/bin/",
      "sudo mkdir --parents /opt/nomad",
      "sudo useradd --system --home /etc/nomad.d --shell /bin/false nomad",
      "sudo mkdir --parents /etc/nomad.d",
      "sudo cp files/nomad/nomad.hcl /etc/nomad.d/",  
      "sudo cp files/nomad/server.hcl /etc/nomad.d/",  
      "sudo chown -R nomad:nomad /opt/nomad",
      "sudo chown -R nomad:nomad /etc/nomad.d",
      "sudo chmod 700 /etc/nomad.d",
      "sudo cp files/nomad.service /usr/lib/systemd/system/nomad.service",  
      "sudo systemctl enable nomad",
      "nomad -autocomplete-install",
      "complete -C /usr/local/bin/nomad nomad",
      "wget https://releases.hashicorp.com/consul/1.10.3/consul_1.10.3_linux_arm64.zip",
      "unzip consul_1.10.3_linux_arm64.zip",
      "sudo chown root:root consul",
      "sudo mv consul /usr/local/bin/",
      "sudo useradd --system --home /etc/consul.d --shell /bin/false consul",
      "sudo mkdir --parents /opt/consul",
      "sudo chown --recursive consul:consul /opt/consul",
      "sudo mkdir --parents /etc/consul.d",  
      "sudo cp files/consul/consul.hcl /etc/consul.d/",  
      "sudo cp files/consul/server.hcl /etc/consul.d/",  
      "sudo chown --recursive consul:consul /etc/consul.d",  
      "sudo chmod 640 /etc/consul.d/*.hcl",  
      "sudo cp files/consul.service /usr/lib/systemd/system/consul.service",  
      "sudo systemctl enable consul",
      "echo 'export NOMAD_ADDR=http://server-001:4646' >> /home/opc/.bashrc",
      "echo 'export NOMAD_ADDR=http://server-001:4646' >> /home/opc/.bashrc",
      "sudo shutdown -r +1"
    ]
  }
}

resource oci_core_instance server-002 {
  agent_config {
    are_all_plugins_disabled = "false"
    is_management_disabled   = "false"
    is_monitoring_disabled   = "false"
    plugins_config {
      desired_state = "DISABLED"
      name          = "Vulnerability Scanning"
    }
    plugins_config {
      desired_state = "ENABLED"
      name          = "OS Management Service Agent"
    }
    plugins_config {
      desired_state = "ENABLED"
      name          = "Compute Instance Run Command"
    }
    plugins_config {
      desired_state = "ENABLED"
      name          = "Compute Instance Monitoring"
    }
    plugins_config {
      desired_state = "DISABLED"
      name          = "Block Volume Management"
    }
    plugins_config {
      desired_state = "DISABLED"
      name          = "Bastion"
    }
  }
  connection {
    type = "ssh"
    # The default username for our AMI
    user        = "opc"
    host        = self.public_ip
    private_key = file("~/.ssh/id_rsa")
    # The connection will use the local SSH agent for authentication.
  }
  #async = <<Optional value not found in discovery>>
  availability_config {
    #is_live_migration_preferred = <<Optional value not found in discovery>>
    recovery_action = "RESTORE_INSTANCE"
  }
  availability_domain = var.oci_identity_availability_domain
  #capacity_reservation_id = <<Optional value not found in discovery>>
  compartment_id = var.compartment_ocid
  create_vnic_details {
    #assign_private_dns_record = <<Optional value not found in discovery>>
    assign_public_ip = "true"
    assign_private_dns_record = "true"
    defined_tags = {
      "Oracle-Tags.CreatedBy" = "PetCluster"
    }
    display_name = "server-002"
    freeform_tags = {
    }
    hostname_label = "server-002"
    nsg_ids = [
    ]
    private_ip             = var.server_002_priv_ip
    skip_source_dest_check = "false"
    subnet_id              = oci_core_subnet.export_subnet-pet_cluster.id
    #vlan_id = <<Optional value not found in discovery>>
  }
  #dedicated_vm_host_id = <<Optional value not found in discovery>>
  defined_tags = {
    "Oracle-Tags.CreatedBy" = "PetCluster"
  }
  display_name = "server-002"
  extended_metadata = {
  }
  #fault_domain = "FAULT-DOMAIN-2"
  freeform_tags = {
  }
  instance_options {
    are_legacy_imds_endpoints_disabled = "false"
  }
  #ipxe_script = <<Optional value not found in discovery>>
  #is_pv_encryption_in_transit_enabled = <<Optional value not found in discovery>>
  launch_options {
    boot_volume_type                    = "PARAVIRTUALIZED"
    firmware                            = "UEFI_64"
    is_consistent_volume_naming_enabled = "true"
    is_pv_encryption_in_transit_enabled = "true"
    network_type                        = "PARAVIRTUALIZED"
    remote_data_volume_type             = "PARAVIRTUALIZED"
  }
  metadata = {
    "ssh_authorized_keys" = var.ssh_public_key
  }
  #preserve_boot_volume = <<Optional value not found in discovery>>
  shape = var.srv_shape
  shape_config {
    baseline_ocpu_utilization = ""
    memory_in_gbs             = var.srv_mem
    ocpus                     = "1"
  }
  source_details {
    #boot_volume_size_in_gbs = <<Optional value not found in discovery>>
    #kms_key_id = <<Optional value not found in discovery>>
    source_id   = var.srv_source_image_id
    source_type = "image"
  }
  state = "RUNNING"
  provisioner "file" {
    source      = "files"
    destination = "/home/opc"
  }
  provisioner "remote-exec" {
    inline = [
      "sudo yum -y update",
      "sudo systemctl stop firewalld",
      "sudo systemctl disable firewalld",
      "sudo sed -i 's/=enforcing/=disabled/' /etc/selinux/config",
      "sudo yum config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo",
      "sudo sed -i 's/$$releasever/8/g' /etc/yum.repos.d/docker-ce.repo",
      "sudo yum install -y docker-ce.aarch64",
      "sudo systemctl enable docker",
      "sudo yum install -y bind",
      "sudo cp files/dns/named.conf /etc/",  
      "sudo cp files/dns/consul.conf /etc/named/",  
      "sudo systemctl enable named",
      "wget https://releases.hashicorp.com/nomad/1.2.2/nomad_1.2.2_linux_arm64.zip",
      "unzip nomad_1.2.2_linux_arm64.zip",
      "sudo chown root:root nomad",
      "sudo mv nomad /usr/local/bin/",
      "sudo mkdir --parents /opt/nomad",
      "sudo useradd --system --home /etc/nomad.d --shell /bin/false nomad",
      "sudo mkdir --parents /etc/nomad.d",
      "sudo cp files/nomad/nomad.hcl /etc/nomad.d/",  
      "sudo cp files/nomad/server.hcl /etc/nomad.d/",  
      "sudo chown -R nomad:nomad /opt/nomad",
      "sudo chown -R nomad:nomad /etc/nomad.d",
      "sudo chmod 700 /etc/nomad.d",
      "sudo cp files/nomad.service /usr/lib/systemd/system/nomad.service",  
      "sudo systemctl enable nomad",
      "nomad -autocomplete-install",
      "complete -C /usr/local/bin/nomad nomad",
      "wget https://releases.hashicorp.com/consul/1.10.3/consul_1.10.3_linux_arm64.zip",
      "unzip consul_1.10.3_linux_arm64.zip",
      "sudo chown root:root consul",
      "sudo mv consul /usr/local/bin/",
      "sudo useradd --system --home /etc/consul.d --shell /bin/false consul",
      "sudo mkdir --parents /opt/consul",
      "sudo chown --recursive consul:consul /opt/consul",
      "sudo mkdir --parents /etc/consul.d",  
      "sudo cp files/consul/consul.hcl /etc/consul.d/",  
      "sudo cp files/consul/server.hcl /etc/consul.d/",  
      "sudo chown --recursive consul:consul /etc/consul.d",  
      "sudo chmod 640 /etc/consul.d/*.hcl",  
      "sudo cp files/consul.service /usr/lib/systemd/system/consul.service",  
      "sudo systemctl enable consul",
      "echo 'export NOMAD_ADDR=http://server-002:4646' >> /home/opc/.bashrc",
      "sudo shutdown -r +1"
    ]
  }
}

resource oci_core_instance server-003 {
  agent_config {
    are_all_plugins_disabled = "false"
    is_management_disabled   = "false"
    is_monitoring_disabled   = "false"
    plugins_config {
      desired_state = "DISABLED"
      name          = "Vulnerability Scanning"
    }
    plugins_config {
      desired_state = "ENABLED"
      name          = "OS Management Service Agent"
    }
    plugins_config {
      desired_state = "ENABLED"
      name          = "Compute Instance Run Command"
    }
    plugins_config {
      desired_state = "ENABLED"
      name          = "Compute Instance Monitoring"
    }
    plugins_config {
      desired_state = "DISABLED"
      name          = "Block Volume Management"
    }
    plugins_config {
      desired_state = "DISABLED"
      name          = "Bastion"
    }
  }
  connection {
    type = "ssh"
    # The default username for our AMI
    user        = "opc"
    host        = self.public_ip
    private_key = file("~/.ssh/id_rsa")
    # The connection will use the local SSH agent for authentication.
  }
  #async = <<Optional value not found in discovery>>
  availability_config {
    #is_live_migration_preferred = <<Optional value not found in discovery>>
    recovery_action = "RESTORE_INSTANCE"
  }
  availability_domain = var.oci_identity_availability_domain
  #capacity_reservation_id = <<Optional value not found in discovery>>
  compartment_id = var.compartment_ocid
  create_vnic_details {
    #assign_private_dns_record = <<Optional value not found in discovery>>
    assign_public_ip = "true"
    assign_private_dns_record = "true"
    defined_tags = {
      "Oracle-Tags.CreatedBy" = "PetCluster"
    }
    display_name = "server-003"
    freeform_tags = {
    }
    hostname_label = "server-003"
    nsg_ids = [
    ]
    private_ip             = var.server_003_priv_ip
    skip_source_dest_check = "false"
    subnet_id              = oci_core_subnet.export_subnet-pet_cluster.id
    #vlan_id = <<Optional value not found in discovery>>
  }
  #dedicated_vm_host_id = <<Optional value not found in discovery>>
  defined_tags = {
    "Oracle-Tags.CreatedBy" = "PetCluster"
  }
  display_name = "server-003"
  extended_metadata = {
  }
#  fault_domain = "FAULT-DOMAIN-3"
  freeform_tags = {
  }
  instance_options {
    are_legacy_imds_endpoints_disabled = "false"
  }
  #ipxe_script = <<Optional value not found in discovery>>
  #is_pv_encryption_in_transit_enabled = <<Optional value not found in discovery>>
  launch_options {
    boot_volume_type                    = "PARAVIRTUALIZED"
    firmware                            = "UEFI_64"
    is_consistent_volume_naming_enabled = "true"
    is_pv_encryption_in_transit_enabled = "true"
    network_type                        = "PARAVIRTUALIZED"
    remote_data_volume_type             = "PARAVIRTUALIZED"
  }
  metadata = {
    "ssh_authorized_keys" = var.ssh_public_key
  }
  #preserve_boot_volume = <<Optional value not found in discovery>>
  shape = var.srv_shape
  shape_config {
    baseline_ocpu_utilization = ""
    memory_in_gbs             = var.cli_mem
    ocpus                     = "1"
  }
  source_details {
    #boot_volume_size_in_gbs = <<Optional value not found in discovery>>
    #kms_key_id = <<Optional value not found in discovery>>
    source_id   = var.srv_source_image_id
    source_type = "image"
  }
  state = "RUNNING"
  provisioner "file" {
    source      = "files"
    destination = "/home/opc"
  }
  provisioner "remote-exec" {
    inline = [
      "sudo yum -y update",
      "sudo systemctl stop firewalld",
      "sudo systemctl disable firewalld",
      "sudo sed -i 's/=enforcing/=disabled/' /etc/selinux/config",
      "sudo yum config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo",
      "sudo sed -i 's/$$releasever/8/g' /etc/yum.repos.d/docker-ce.repo",
      "sudo yum install -y docker-ce.aarch64",
      "sudo yum install -y wireguard",
      "sudo cp files/vpn.modules /etc/sysconfig/modules",  
      "sudo systemctl enable docker",
      "sudo yum install -y bind",
      "sudo cp files/dns/named.conf /etc/",  
      "sudo cp files/dns/consul.conf /etc/named/",  
      "sudo mkdir /etc/docker",
      "sudo cp files/dns/daemon.json /etc/docker/",
      "sudo systemctl enable named",
      "wget https://releases.hashicorp.com/nomad/1.2.2/nomad_1.2.2_linux_arm64.zip",
      "unzip nomad_1.2.2_linux_arm64.zip",
      "sudo chown root:root nomad",
      "sudo mv nomad /usr/local/bin/",
      "sudo mkdir --parents /opt/nomad",
      "sudo useradd --system --home /etc/nomad.d --shell /bin/false nomad",
      "sudo mkdir --parents /etc/nomad.d",
      "sudo cp files/nomad/nomad.hcl /etc/nomad.d/",  
      "sudo cp files/nomad/client.hcl /etc/nomad.d/",  
      "sudo cp files/volumes/*.hcl /etc/nomad.d/",  
      "sudo mkdir --parents /mnt/volumes/v_d_mongodb",
      "sudo mkdir /mnt/volumes/v_d_wireguard-config",
      "sudo mkdir /mnt/volumes/v_d_postgres-data",
      "sudo chown -R opc /mnt/volumes/",
      "sudo chmod 700 /etc/nomad.d",
      "sudo cp files/nomad.client.service /usr/lib/systemd/system/nomad.service",  
      "sudo systemctl enable nomad",
      "nomad -autocomplete-install",
      "complete -C /usr/local/bin/nomad nomad",
      "wget https://releases.hashicorp.com/consul/1.10.3/consul_1.10.3_linux_arm64.zip",
      "unzip consul_1.10.3_linux_arm64.zip",
      "sudo chown root:root consul",
      "sudo mv consul /usr/local/bin/",
      "sudo useradd --system --home /etc/consul.d --shell /bin/false consul",
      "sudo mkdir --parents /opt/consul",
      "sudo chown --recursive consul:consul /opt/consul",
      "sudo mkdir --parents /etc/consul.d",  
      "sudo cp files/consul/consul.hcl /etc/consul.d/",  
      "sudo cp files/consul/client.hcl /etc/consul.d/",  
      "sudo chown --recursive consul:consul /etc/consul.d",  
      "sudo chmod 640 /etc/consul.d/*.hcl",  
      "sudo cp files/consul.service /usr/lib/systemd/system/consul.service",  
      "sudo systemctl enable consul",
      "wget https://github.com/containernetworking/plugins/releases/download/v1.0.1/cni-plugins-linux-arm64-v1.0.1.tgz",
      "sudo mkdir -p /opt/cni/bin",
      "sudo tar -xzf cni-plugins-linux-arm64-v1.0.1.tgz -C /opt/cni/bin",
      "echo 'export NOMAD_ADDR=http://server-003:4646' >> /home/opc/.bashrc",
      "cp /etc/sysctl.conf /tmp/",
      "sudo echo 'vm.max_map_count=262144' >> /tmp/sysctl.conf",
      "sudo echo 'net.ipv4.conf.all.route_localnet = 1' >> /tmp/sysctl.conf",
      "sudo echo 'net.bridge.bridge-nf-call-arptables = 1' >> /tmp/sysctl.conf",
      "sudo echo 'net.bridge.bridge-nf-call-ip6tables = 1' >> /tmp/sysctl.conf",
      "sudo echo 'net.bridge.bridge-nf-call-iptables = 1' >> /tmp/sysctl.conf",
      "sudo cp /tmp/sysctl.conf /etc/",
      "sudo shutdown -r +1"
    ]
  }
}

resource oci_core_instance server-004 {
  agent_config {
    are_all_plugins_disabled = "false"
    is_management_disabled   = "false"
    is_monitoring_disabled   = "false"
    plugins_config {
      desired_state = "DISABLED"
      name          = "Vulnerability Scanning"
    }
    plugins_config {
      desired_state = "ENABLED"
      name          = "OS Management Service Agent"
    }
    plugins_config {
      desired_state = "ENABLED"
      name          = "Compute Instance Run Command"
    }
    plugins_config {
      desired_state = "ENABLED"
      name          = "Compute Instance Monitoring"
    }
    plugins_config {
      desired_state = "DISABLED"
      name          = "Block Volume Management"
    }
    plugins_config {
      desired_state = "DISABLED"
      name          = "Bastion"
    }
  }
  connection {
    type = "ssh"
    # The default username for our AMI
    user        = "opc"
    host        = self.public_ip
    private_key = file("~/.ssh/id_rsa")
    # The connection will use the local SSH agent for authentication.
  }
  #async = <<Optional value not found in discovery>>
  availability_config {
    #is_live_migration_preferred = <<Optional value not found in discovery>>
    recovery_action = "RESTORE_INSTANCE"
  }
  availability_domain = var.oci_identity_availability_domain
  #capacity_reservation_id = <<Optional value not found in discovery>>
  compartment_id = var.compartment_ocid
  create_vnic_details {
    #assign_private_dns_record = <<Optional value not found in discovery>>
    assign_public_ip = "true"
    assign_private_dns_record = "true"
    defined_tags = {
      "Oracle-Tags.CreatedBy" = "PetCluster"
    }
    display_name = "server-004"
    freeform_tags = {
    }
    hostname_label = "server-004"
    nsg_ids = [
    ]
    private_ip             = var.server_004_priv_ip
    skip_source_dest_check = "false"
    subnet_id              = oci_core_subnet.export_subnet-pet_cluster.id
    #vlan_id = <<Optional value not found in discovery>>
  }
  #dedicated_vm_host_id = <<Optional value not found in discovery>>
  defined_tags = {
    "Oracle-Tags.CreatedBy" = "PetCluster"
  }
  display_name = "server-004"
  extended_metadata = {
  }
  #fault_domain = "FAULT-DOMAIN-1"
  freeform_tags = {
  }
  instance_options {
    are_legacy_imds_endpoints_disabled = "false"
  }
  #ipxe_script = <<Optional value not found in discovery>>
  #is_pv_encryption_in_transit_enabled = <<Optional value not found in discovery>>
  launch_options {
    boot_volume_type                    = "PARAVIRTUALIZED"
    firmware                            = "UEFI_64"
    is_consistent_volume_naming_enabled = "true"
    is_pv_encryption_in_transit_enabled = "true"
    network_type                        = "PARAVIRTUALIZED"
    remote_data_volume_type             = "PARAVIRTUALIZED"
  }
  metadata = {
    "ssh_authorized_keys" = var.ssh_public_key
  }
  #preserve_boot_volume = <<Optional value not found in discovery>>
  shape = var.srv_shape
  shape_config {
    baseline_ocpu_utilization = ""
    memory_in_gbs             = var.cli_mem
    ocpus                     = "1"
  }
  source_details {
    #boot_volume_size_in_gbs = <<Optional value not found in discovery>>
    #kms_key_id = <<Optional value not found in discovery>>
    source_id   = var.srv_source_image_id
    source_type = "image"
  }
  state = "RUNNING"
  provisioner "file" {
    source      = "files"
    destination = "/home/opc"
  }
  provisioner "remote-exec" {
    inline = [
      "sudo yum -y update",
      "sudo systemctl stop firewalld",
      "sudo systemctl disable firewalld",
      "sudo sed -i 's/=enforcing/=disabled/' /etc/selinux/config",
      "sudo yum config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo",
      "sudo sed -i 's/$$releasever/8/g' /etc/yum.repos.d/docker-ce.repo",
      "sudo yum install -y docker-ce.aarch64",
      "sudo yum install -y wireguard",
      "sudo cp files/vpn.modules /etc/sysconfig/modules",  
      "sudo systemctl enable docker",
      "sudo yum install -y bind",
      "sudo cp files/dns/named.conf /etc/",  
      "sudo cp files/dns/consul.conf /etc/named/",  
      "sudo mkdir /etc/docker",
      "sudo cp files/dns/daemon.json /etc/docker/",
      "sudo systemctl enable named",
      "wget https://releases.hashicorp.com/nomad/1.2.2/nomad_1.2.2_linux_arm64.zip",
      "unzip nomad_1.2.2_linux_arm64.zip",
      "sudo chown root:root nomad",
      "sudo mv nomad /usr/local/bin/",
      "sudo mkdir --parents /opt/nomad",
      "sudo useradd --system --home /etc/nomad.d --shell /bin/false nomad",
      "sudo mkdir --parents /etc/nomad.d",
      "sudo cp files/nomad/nomad.hcl /etc/nomad.d/",  
      "sudo cp files/nomad/client.hcl /etc/nomad.d/",  
      "sudo cp files/volumes/*.hcl /etc/nomad.d/",  
      "sudo mkdir --parents /mnt/volumes/v_d_mongodb",
      "sudo mkdir /mnt/volumes/v_d_wireguard-config",
      "sudo mkdir /mnt/volumes/v_d_postgres-data",
      "sudo chown -R opc /mnt/volumes/",
      "sudo chmod 700 /etc/nomad.d",
      "sudo cp files/nomad.client.service /usr/lib/systemd/system/nomad.service",  
      "sudo systemctl enable nomad",
      "nomad -autocomplete-install",
      "complete -C /usr/local/bin/nomad nomad",
      "wget https://releases.hashicorp.com/consul/1.10.3/consul_1.10.3_linux_arm64.zip",
      "unzip consul_1.10.3_linux_arm64.zip",
      "sudo chown root:root consul",
      "sudo mv consul /usr/local/bin/",
      "sudo useradd --system --home /etc/consul.d --shell /bin/false consul",
      "sudo mkdir --parents /opt/consul",
      "sudo chown --recursive consul:consul /opt/consul",
      "sudo mkdir --parents /etc/consul.d",  
      "sudo cp files/consul/consul.hcl /etc/consul.d/",  
      "sudo cp files/consul/client.hcl /etc/consul.d/",  
      "sudo chown --recursive consul:consul /etc/consul.d",  
      "sudo chmod 640 /etc/consul.d/*.hcl",  
      "sudo cp files/consul.service /usr/lib/systemd/system/consul.service",  
      "sudo systemctl enable consul",
      "wget https://github.com/containernetworking/plugins/releases/download/v1.0.1/cni-plugins-linux-arm64-v1.0.1.tgz",
      "sudo mkdir -p /opt/cni/bin",
      "sudo tar -xzf cni-plugins-linux-arm64-v1.0.1.tgz -C /opt/cni/bin",
      "echo 'export NOMAD_ADDR=http://server-004:4646' >> /home/opc/.bashrc",
      "cp /etc/sysctl.conf /tmp/",
      "sudo echo 'vm.max_map_count=262144' >> /tmp/sysctl.conf",
      "sudo echo 'net.ipv4.conf.all.route_localnet = 1' >> /tmp/sysctl.conf",
      "sudo echo 'net.bridge.bridge-nf-call-arptables = 1' >> /tmp/sysctl.conf",
      "sudo echo 'net.bridge.bridge-nf-call-ip6tables = 1' >> /tmp/sysctl.conf",
      "sudo echo 'net.bridge.bridge-nf-call-iptables = 1' >> /tmp/sysctl.conf",
      "sudo cp /tmp/sysctl.conf /etc/",
      "sudo shutdown -r +1"
    ]
  }
}

resource oci_core_internet_gateway export_Internet-Gateway-vcn-pet_cluster {
  compartment_id = var.compartment_ocid
  defined_tags = {
    "Oracle-Tags.CreatedBy" = "PetCluster"
  }
  display_name = "Internet Gateway vcn-pet_cluster"
  enabled      = "true"
  freeform_tags = {
  }
  vcn_id = oci_core_vcn.export_vcn-pet_cluster.id
}
resource oci_core_subnet export_subnet-pet_cluster {
  #availability_domain = <<Optional value not found in discovery>>
  cidr_block     = "172.16.0.0/24"
  compartment_id = var.compartment_ocid
  defined_tags = {
    "Oracle-Tags.CreatedBy" = "PetCluster"
  }
  dhcp_options_id = oci_core_vcn.export_vcn-pet_cluster.default_dhcp_options_id
  display_name    = "subnet-pet_cluster"
  dns_label       = "subnet11052106"
  freeform_tags = {
  }
  #ipv6cidr_block = <<Optional value not found in discovery>>
  prohibit_internet_ingress  = "false"
  prohibit_public_ip_on_vnic = "false"
  route_table_id             = oci_core_vcn.export_vcn-pet_cluster.default_route_table_id
  security_list_ids = [
    oci_core_vcn.export_vcn-pet_cluster.default_security_list_id,
  ]
  vcn_id = oci_core_vcn.export_vcn-pet_cluster.id
}

#resource oci_core_private_ip export_server-001 {
#  defined_tags = {
#    "Oracle-Tags.CreatedBy" = "PetCluster"
#  }
#  display_name = "server-001"
#  freeform_tags = {
#  }
#  hostname_label = "server-001"
#  ip_address     = var.server_001_priv_ip
#  #vlan_id = <<Optional value not found in discovery>>
#  #vnic_id = "ocid1.vnic.oc1.iad.abuwcljroy2wx3st2z34ooqmazmlhu5y6d3ctcmpxx4oi3wpwjcicx42ol5a"
#}
#
#resource oci_core_private_ip export_server-002 {
#  defined_tags = {
#    "Oracle-Tags.CreatedBy" = "PetCluster"
#  }
#  display_name = "server-002"
#  freeform_tags = {
#  }
#  hostname_label = "server-002"
#  ip_address     = var.server_002_priv_ip
#  #vlan_id = <<Optional value not found in discovery>>
#  #vnic_id = "ocid1.vnic.oc1.iad.abuwcljth2azwr5jvi6axmlpkjactfwxeuwp4hblnrcooq72w4lhcebv237q"
#}
#
#resource oci_core_private_ip export_server-003 {
#  defined_tags = {
#    "Oracle-Tags.CreatedBy" = "PetCluster"
#  }
#  display_name = "server-003"
#  freeform_tags = {
#  }
#  hostname_label = "server-003"
#  ip_address     = var.server_003_priv_ip
#  #vlan_id = <<Optional value not found in discovery>>
#  #vnic_id = "ocid1.vnic.oc1.iad.abuwcljtgdctfppcewdjo55oiv2efxvu722ylljqn6yylcxnbqvqta4kjjjq"
#}

#resource oci_core_private_ip export_server-004 {
#  defined_tags = {
#    "Oracle-Tags.CreatedBy" = "PetCluster"
#  }
#  display_name = "server-004"
#  freeform_tags = {
#  }
#  hostname_label = "server-004"
#  ip_address     = var.server_004_priv_ip
#  #vlan_id = <<Optional value not found in discovery>>
#  #vnic_id = "ocid1.vnic.oc1.iad.abuwcljswkt72jf5xkqkbcapjjdfqqlslpkpqri3b2uiw4pmko4lhhv2mwlq"
#}

resource oci_core_vcn export_vcn-pet_cluster {
  #cidr_block = <<Optional value not found in discovery>>
  cidr_blocks = [
    "172.16.0.0/16",
  ]
  compartment_id = var.compartment_ocid
  defined_tags = {
    "Oracle-Tags.CreatedBy" = "PetCluster"
  }
  display_name = "vcn-pet_cluster"
  dns_label    = "petcluster"
  freeform_tags = {
  }
 is_ipv6enabled = false
}
resource oci_core_default_dhcp_options export_Default-DHCP-Options-for-vcn-pet_cluster {
  compartment_id = var.compartment_ocid
  defined_tags = {
    "Oracle-Tags.CreatedBy" = "buscajuris"
  }
  display_name     = "Default DHCP Options for vcn-pet_cluster"
  domain_name_type = "CUSTOM_DOMAIN"
  freeform_tags = {
  }
  manage_default_resource_id = oci_core_vcn.export_vcn-pet_cluster.default_dhcp_options_id
  options {
    custom_dns_servers = [  "127.0.0.1", "169.254.169.254",
    ]
    #search_domain_names = <<Optional value not found in discovery>>
    server_type = "CustomDnsServer"
    type        = "DomainNameServer"
  }
  options {
    #custom_dns_servers = <<Optional value not found in discovery>>
    search_domain_names = [
      "petcluster.oraclevcn.com",
    ]
    #server_type = <<Optional value not found in discovery>>
    type = "SearchDomain"
  }
}

resource oci_core_default_route_table export_Default-Route-Table-for-vcn-pet_cluster {
  compartment_id = var.compartment_ocid
  defined_tags = {
    "Oracle-Tags.CreatedBy" = "PetCluster"
  }
  display_name = "Default Route Table for vcn-pet_cluster"
  freeform_tags = {
  }
  manage_default_resource_id = oci_core_vcn.export_vcn-pet_cluster.default_route_table_id
  route_rules {
    #description = <<Optional value not found in discovery>>
    destination       = "0.0.0.0/0"
    destination_type  = "CIDR_BLOCK"
    network_entity_id = oci_core_internet_gateway.export_Internet-Gateway-vcn-pet_cluster.id
  }
}

resource oci_core_default_security_list export_Default-Security-List-for-vcn-pet_cluster {
  compartment_id = var.compartment_ocid
  defined_tags = {
    "Oracle-Tags.CreatedBy" = "PetCluster"
  }
  display_name = "Default Security List for vcn-pet_cluster"
  egress_security_rules {
    #description = <<Optional value not found in discovery>>
    destination      = "0.0.0.0/0"
    destination_type = "CIDR_BLOCK"
    #icmp_options = <<Optional value not found in discovery>>
    protocol  = "all"
    stateless = "false"
    #tcp_options = <<Optional value not found in discovery>>
    #udp_options = <<Optional value not found in discovery>>
  }
  freeform_tags = {
  }
  ingress_security_rules {
    #description = <<Optional value not found in discovery>>
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = "0.0.0.0/0"
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "22"
      min = "22"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  ingress_security_rules {
    #description = <<Optional value not found in discovery>>
    icmp_options {
      code = "4"
      type = "3"
    }
    protocol    = "1"
    source      = "0.0.0.0/0"
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    #tcp_options = <<Optional value not found in discovery>>
    #udp_options = <<Optional value not found in discovery>>
  }
  ingress_security_rules {
    #description = <<Optional value not found in discovery>>
    icmp_options {
      code = "-1"
      type = "3"
    }
    protocol    = "1"
    source      = "172.16.0.0/16"
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    #tcp_options = <<Optional value not found in discovery>>
    #udp_options = <<Optional value not found in discovery>>
  }
  ingress_security_rules {
    #description = <<Optional value not found in discovery>>
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = "172.16.0.0/24"
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "4647"
      min = "4647"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  ingress_security_rules {
    #description = <<Optional value not found in discovery>>
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = "172.16.0.0/24"
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "4648"
      min = "4648"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  ingress_security_rules {
    #description = <<Optional value not found in discovery>>
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "17"
    source      = "172.16.0.0/24"
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    #tcp_options = <<Optional value not found in discovery>>
    udp_options {
      max = "4648"
      min = "4648"
      #source_port_range = <<Optional value not found in discovery>>
    }
  }
  ingress_security_rules {
    #description = <<Optional value not found in discovery>>
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = "172.16.0.0/24"
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "8500"
      min = "8500"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  ingress_security_rules {
    #description = <<Optional value not found in discovery>>
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = "172.16.0.0/24"
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "8600"
      min = "8600"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  ingress_security_rules {
    #description = <<Optional value not found in discovery>>
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "17"
    source      = "172.16.0.0/24"
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    #tcp_options = <<Optional value not found in discovery>>
    udp_options {
      max = "8600"
      min = "8600"
      #source_port_range = <<Optional value not found in discovery>>
    }
  }
ingress_security_rules {
    #description = <<Optional value not found in discovery>>
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "17"
    source      = "0.0.0.0/0"
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    #tcp_options = <<Optional value not found in discovery>>
    udp_options {
      max = "51820"
      min = "51820"
      #source_port_range = <<Optional value not found in discovery>>
    }
  }

  ingress_security_rules {
    #description = <<Optional value not found in discovery>>
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "17"
    source      = "172.16.0.0/24"
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    #tcp_options = <<Optional value not found in discovery>>
    udp_options {
      max = "8302"
      min = "8301"
      #source_port_range = <<Optional value not found in discovery>>
    }
  }
  ingress_security_rules {
    #description = <<Optional value not found in discovery>>
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = "172.16.0.0/24"
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "8302"
      min = "8300"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  ingress_security_rules {
    #description = <<Optional value not found in discovery>>
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = var.my_internet_ip
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "8500"
      min = "8500"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  ingress_security_rules {
    #description = <<Optional value not found in discovery>>
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = var.my_internet_ip
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "4646"
      min = "4646"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  ingress_security_rules {
    #description = <<Optional value not found in discovery>>
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = "172.16.0.0/24"
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "32000"
      min = "20000"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
ingress_security_rules {
    #description = <<Optional value not found in discovery>>
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = "172.16.0.0/24"
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "9999"
      min = "9998"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
ingress_security_rules {
    #description = <<Optional value not found in discovery>>
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = var.my_internet_ip
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "9999"
      min = "9998"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
}

ingress_security_rules {
    #description = <<Optional value not found in discovery>>
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = "0.0.0.0/0"
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "80"
      min = "80"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  manage_default_resource_id = oci_core_vcn.export_vcn-pet_cluster.default_security_list_id
}
## This configuration was generated by terraform-provider-oci

resource oci_load_balancer_load_balancer export_lb_ocijuris {
  compartment_id = var.compartment_ocid
  defined_tags = {
    "Oracle-Tags.CreatedBy" = "PetCluster"
    "Oracle-Tags.CreatedOn" = "2021-12-05T02:27:33.152Z"
  }
  display_name = "lb_ocijuris"
  freeform_tags = {
  }
  ip_mode    = "IPV4"
  is_private = "false"
  network_security_group_ids = [
  ]
  reserved_ips  {
    id = var.load_balancer_reserved_ips_id
  }
  shape = "flexible"
  shape_details {
    maximum_bandwidth_in_mbps = "10"
    minimum_bandwidth_in_mbps = "10"
  }
  subnet_ids = [
    oci_core_subnet.export_subnet-pet_cluster.id,
    ]
}

resource oci_load_balancer_backend_set export_bs_lb_2021-1204-2324 {
  health_checker {
    interval_ms         = "10000"
    port                = "9998"
    protocol            = "HTTP"
    response_body_regex = ""
    retries             = "3"
    return_code         = "200"
    timeout_in_millis   = "3000"
    url_path            = "/health"
  }
  load_balancer_id = oci_load_balancer_load_balancer.export_lb_ocijuris.id
  name             = "bs_lb_2021-1204-2324"
  policy           = "ROUND_ROBIN"
}

resource oci_load_balancer_backend export_172-16-0-104-9999 {
  backendset_name  = oci_load_balancer_backend_set.export_bs_lb_2021-1204-2324.name
  backup           = "false"
  drain            = "false"
  ip_address       = "172.16.0.104"
  load_balancer_id = oci_load_balancer_load_balancer.export_lb_ocijuris.id
  offline          = "false"
  port             = "9999"
  weight           = "1"
}

resource oci_load_balancer_backend export_172-16-0-103-9999 {
  backendset_name  = oci_load_balancer_backend_set.export_bs_lb_2021-1204-2324.name
  backup           = "false"
  drain            = "false"
  ip_address       = "172.16.0.103"
  load_balancer_id = oci_load_balancer_load_balancer.export_lb_ocijuris.id
  offline          = "false"
  port             = "9999"
  weight           = "1"
}

resource oci_load_balancer_listener export_lb_ocijuris_listener_lb_2021-1204-2324 {
  connection_configuration {
    backend_tcp_proxy_protocol_version = "0"
    idle_timeout_in_seconds            = "60"
  }
  default_backend_set_name = oci_load_balancer_backend_set.export_bs_lb_2021-1204-2324.name
  hostname_names = [
  ]
  load_balancer_id = oci_load_balancer_load_balancer.export_lb_ocijuris.id
  name             = "listener_lb_2021-1204-2324"
  #path_route_set_name = <<Optional value not found in discovery>>
  port     = "80"
  protocol = "HTTP"
  #routing_policy_name = <<Optional value not found in discovery>>
  rule_set_names = [
  ]
}

