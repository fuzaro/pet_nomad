client {
  enabled = true
  cni_path = "/opt/cni/bin"
  bridge_network_name = "nomad"
  bridge_network_subnet = "10.42.0.0/16"
}
plugin "docker" {
  config {
    allow_privileged = true
    volumes {
      enabled      = true
    }
  }
}
