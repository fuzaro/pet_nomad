server {
  enabled = true
  bootstrap_expect = 2
}
acl {
  enabled = false
}
advertise {
    http = "0.0.0.0"
}
