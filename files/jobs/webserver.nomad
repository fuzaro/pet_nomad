job "www" {
  datacenters = ["oci1"]
  type = "service"
  update {
    max_parallel = 1
    min_healthy_time = "10s"
    healthy_deadline = "3m"
    progress_deadline = "10m"
    auto_revert = false
    canary = 0
  }
  migrate {
    max_parallel = 1
    health_check = "checks"
    min_healthy_time = "10s"
    healthy_deadline = "5m"
  }
  group "cache" {
    count = 1
    network {
      mode = "bridge"
      port "http" {
        to = 80
      }
    }
    service {
      name = "www-server"
      tags = ["www", "webserver", "urlprefix-/webserver strip=/webserver"]
      port = "http"
  check {
        type = "http"
        path = "/"
        interval = "5s"
        timeout = "2s"
      }

    }
    restart {
      attempts = 2
      interval = "30m"
      delay = "15s"
      mode = "fail"
    }
    ephemeral_disk {
      size = 300
    }
    task "nginx" {
      driver = "docker"
      config {
        image = "nginx"
        ports = ["http"]
      }
      resources {
      }
    }
  }
}
