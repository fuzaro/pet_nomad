job "rstudio" {
  datacenters = ["oci1"]
  type = "service"
  update {
    max_parallel = 1
    min_healthy_time = "10s"
    healthy_deadline = "3m"
    progress_deadline = "10m"
    auto_revert = false
    canary = 0
  }
  migrate {
    max_parallel = 1
    health_check = "checks"
    min_healthy_time = "10s"
    healthy_deadline = "5m"
  }
  group "dscience" {
    count = 1
    network {
      mode = "bridge"
      port "rstudio" {
        to = 8787
      }
    }
    service {
      name = "rstudio-server"
      tags = ["rstudio-server", "rstudio", "urlprefix-/rstudio-server strip=/rstudio-server"]
      port = "rstudio"
  check {
        type = "http"
        path = "/"
        interval = "5s"
        timeout = "2s"
      }

    }
    restart {
      attempts = 2
      interval = "30m"
      delay = "15s"
      mode = "fail"
    }
    ephemeral_disk {
      size = 300
    }
    task "rstudio" {
      driver = "docker"
      config {
        image = "ghcr.io/lescai-teaching/rstudio-docker-arm64:latest"
        ports = ["rstudio"]
      }
    env {
           PASSWORD = "admin"
        }
    affinity {
      attribute = "${node.unique.name}"
      value     = "server-004"
      weight    = 100
    }

      resources {
      }
    }
  }
}
