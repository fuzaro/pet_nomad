job "wireguard" {
 datacenters = ["oci1"]
 type = "service"

group "vpn" {
    count = 1
    network {
              mode = "host"
                port "udp_vpn" {
		  static = 51820 
		}
        }
    volume "v_d_wireguard-config" {
      type = "host"
      read_only = false
      source = "v_d_wireguard-config"
    }
    volume "v_d_wireguard-modules" {
      type = "host"
      read_only = false
      source = "v_d_wireguard-modules"
    }
    task "wireguard" {
        driver = "docker"
        volume_mount {
           volume      = "v_d_wireguard-config"
           destination = "/config"
        }
        volume_mount {
           volume      = "v_d_wireguard-modules"
           destination = "/lib/modules"
        }
        config {
            image = "lscr.io/linuxserver/wireguard"
            privileged = "true"
            ports = ["udp_vpn"]
#            cap_add = ["net_admin", "sys_module"]
            sysctl = {    "net.ipv4.conf.all.src_valid_mark" = "1"  }
        }
        env {
             PUID = "1000"
             PGID = "1000"
             TZ = "America/SaoPaulo"
#             SERVERURL=wireguard.domain.com `#optional`
             SERVERPORT = "51820" 
              PEERS = "2" 
             PEERDNS = "8.8.8.8"
#             INTERNAL_SUBNET=10.13.13.0 `#optional`
             ALLOWEDIPS = "172.16.0.0/24,10.13.13.1/32"
        }
    }
        service {
        name = "wireguard"
        port = "udp_vpn"
        tags = [ "wireguard" ]

#       check {
#        type = "tcp"
#        port = "udp_vpn"
#        interval = "5s"
#        timeout = "2s"
#      }
        }


}
}
