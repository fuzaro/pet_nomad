job "mongodb" {
  datacenters = ["oci1"]
  type = "service"
  update {
    max_parallel = 1
    min_healthy_time = "10s"
    healthy_deadline = "3m"
    progress_deadline = "10m"
    auto_revert = false
    canary = 0
  }
  migrate {
    max_parallel = 1
    health_check = "checks"
    min_healthy_time = "10s"
    healthy_deadline = "5m"
  }
  group "mongodb" {
    count = 1
    network {
        mode = "bridge"
        port "mdb_rest" { to = 27017 }
    }
#volume
    volume "v_d_mongodb" {
      type = "host"
      read_only = false
      source = "v_d_mongodb"
    }
    service {
      name = "mongodb"
      tags = ["mongodb", "index", "urlprefix-/mongodb strip=/mongodb"]
      port = "mdb_rest"
      check {
        type = "http"
        path = "/"
        interval = "5s"
        timeout = "2s"
      }
     }
    restart {
      attempts = 2
      interval = "30m"
      delay = "15s"
      mode = "fail"
    }
    ephemeral_disk {
      size = 300
    }
    task "mongodb" {
      driver = "docker"
    volume_mount {
      volume      = "v_d_mongodb"
      destination = "/data/db"
    }
    config {
      image = "mongo:latest"
    }
    logs {
      max_files     = 5
      max_file_size = 15
    }
    resources {
      cpu = 500
      memory = 512
    }
  }
  }
}
