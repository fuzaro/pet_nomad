job "nginx" {
 datacenters = ["oci1"]
 type = "service"

group "webs" {
    count = 1
    network {
              mode = "bridge"
                port "http" {
		  to = 80 
		}
        }
    task "frontend" {
        driver = "docker"
        config {
            image = "nginx"
            ports = ["http"]
        }
        env {
            DB_HOST = "db03.example.com"
            DB_USER = "web"        
            DB_PASS = "loremipsum"
        }
    }
        service {
        name = "nginx"
        port = "http"
        tags = [ "webserver", "urlprefix-/nginx strip=/nginx" ]

       check {
        type = "http"
        path = "/"
        interval = "5s"
        timeout = "2s"
      }
        }


}
}
