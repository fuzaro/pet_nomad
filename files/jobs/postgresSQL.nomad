#To Configure vault
# vault secrets enable database
# vault write database/config/postgresql  plugin_name=postgresql-database-plugin   connection_url="postgresql://{{username}}:{{password}}@postgres.service.consul:5432/postgres?sslmode=disable"   allowed_roles="*"     username="root"     password="rootpassword"
# vault write database/roles/readonly db_name=postgresql     creation_statements=@readonly.sql     default_ttl=1h max_ttl=24h

job "postgres" {
  datacenters = ["oci1"]
  type = "service"

  group "postgres" {
    count = 1

        network { 
	mode = "bridge"
        port "db" {
          to = 5432
        }
     }
     volume "v_d_postgres-data" {
      type      = "host"
      read_only = false
      source    = "v_d_postgres-data"
    }
    task "postgres" {
      driver = "docker"
        volume_mount {
        volume      = "v_d_postgres-data"
        destination = "/var/lib/postgresql/data"
        read_only   = false      }
      config {
        image = "postgres:13.2"

      }
      env {
          POSTGRES_USER="postgres",
          POSTGRES_PASSWORD="xpostgresx"
      }

      logs {
        max_files     = 5
        max_file_size = 15
      }

      resources {
        cpu = 500
        memory = 512
      }
      service {
        name = "postgres"
        tags = ["postgres for vault", "urlprefix-:5432 proto=tcp"]
        port = "db"

        check {
          name     = "alive"
          type     = "tcp"
          interval = "10s"
          timeout  = "2s"
        }
      }
    }
    restart {
      attempts = 10
      interval = "5m"
      delay = "25s"
      mode = "delay"
    }

  }

  update {
    max_parallel = 1
    min_healthy_time = "5s"
    healthy_deadline = "3m"
    auto_revert = false
    canary = 0
  }
}
