job "elk" {
  datacenters = ["oci1"]
  type = "service"
  update {
    max_parallel = 1
    min_healthy_time = "10s"
    healthy_deadline = "3m"
    progress_deadline = "10m"
    auto_revert = false
    canary = 0
  }
  migrate {
    max_parallel = 1
    health_check = "checks"
    min_healthy_time = "10s"
    healthy_deadline = "5m"
  }
  group "crawler" {
    count = 1
    network {
        mode = "bridge"
      port "es_rest" { to = 9200 }
      port "es_node" { to = 9300 }
    }
#volume
    volume "v_elastic" {
      type      = "host"
      read_only = false
      source    = "v_elastic"
    }
    service {
      name = "elasticsearch"
      tags = ["elastic", "index", "urlprefix-/elasticsearch strip=/elasticsearch"]
      port = "es_rest"
      check {
        type = "http"
        path = "/"
        interval = "5s"
        timeout = "2s"
      }
    }
    restart {
      attempts = 2
      interval = "30m"
      delay = "15s"
      mode = "fail"
    }
    ephemeral_disk {
      size = 300
    }
    task "elastic" {
      env {
         "node.name" = "elasticsearch"
         "cluster.name" = "es-docker-cluster"
#         "cluster.initial_master_nodes" = "elasticsearch"
         "bootstrap.memory_lock" = "true"
         "ES_JAVA_OPTS" = "-Xms512m -Xmx512m"
	 "discovery.type" = "single-node"
      }
      driver = "docker"
#volume_mount
      volume_mount {
        volume      = "v_elastic"
        destination = "/usr/share/elasticsearch/data"
        read_only   = false      }
      config {
        image = "docker.elastic.co/elasticsearch/elasticsearch:7.15.1"
        ports = ["es_rest", "es_node"]
        ulimit {
          memlock = "-1"
          nofile = "65536"
          nproc = "65536"
        }
#	volumes  [
#		"/mnt/elastic/vol_elastic/data:/usr/share/elasticsearch/data"
#        ]
      }
      resources {
        cpu    = 500 # 500 MHz
        memory = 2048 # 256MB
      }
    }
  }
}
