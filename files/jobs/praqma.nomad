job "praqma" {
 datacenters = ["oci1"]
 type = "system"

group "webs" {
    network {
              mode = "bridge"
                port "http" {
                  to = 80
                }
        }
    task "net-tools" {
        driver = "docker"
        config {
            image = "praqma/network-multitool"
            ports = ["http"]
        }
#        env {
#            DB_HOST = "db03.example.com"
#            DB_USER = "web"
#            DB_PASS = "loremipsum"
#        }
    }
        service {
        name = "net-tools"
        port = "http"
        tags = [ "net-tools", "urlprefix-/net-tools strip=/net-tools" ]

       check {
        type = "http"
        path = "/"
        interval = "5s"
        timeout = "2s"
      }
        }


}
}
