client {
host_volume "v_d_mongodb" {
    path      = "/mnt/volumes/v_d_mongodb"
    read_only = false
  }
}
