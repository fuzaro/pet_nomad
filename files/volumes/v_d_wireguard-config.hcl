client {
host_volume "v_d_wireguard-config" {
    path      = "/mnt/volumes/v_d_wireguard-config"
    read_only = false
  }
}
