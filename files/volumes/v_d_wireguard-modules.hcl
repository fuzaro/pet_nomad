client {
host_volume "v_d_wireguard-modules" {
    path      = "/lib/modules"
    read_only = false
  }
}
